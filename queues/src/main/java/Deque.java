import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * User: Alex
 * Date: 16.02.17
 */
public class Deque<Item> implements Iterable<Item> {

    private Node<Item> first = null;
    private Node<Item> last = null;
    private int size = 0;

    /**
     * construct an empty deque
     */
    public Deque() {

    }

    /**
     * is the deque empty?
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * return the number of items on the deque
     */
    public int size() {
        return size;
    }

    /**
     * add the item to the front
     */
    public void addFirst(Item item) {
        if (item == null) {
            throw new NullPointerException("Can't add null to the deque.");
        }

        if (first == null) {
            Node<Item> newFirst = new Node<>();
            newFirst.value = item;
            first = newFirst;
            last = newFirst;
        } else {
            Node<Item> newFirst = first;
            first = new Node<>();
            first.value = item;
            first.next = newFirst;
            newFirst.previous = first;
        }

        size++;
    }

    /**
     * add the item to the end
     */
    public void addLast(Item item) {
        if (item == null) {
            throw new NullPointerException("Can't add null to the deque.");
        }

        if (last == null) {
            Node<Item> newLast = new Node<>();
            newLast.value = item;
            first = newLast;
            last = newLast;
        } else {
            Node<Item> oldLast = last;
            last = new Node<>();
            last.value = item;
            last.previous = oldLast;
            oldLast.next = last;
        }

        size++;    }

    /**
     * remove and return the item from the front
     */
    public Item removeFirst() {
        if (first == null) {
            throw new NoSuchElementException("Client tries to remove an Item from empty deque.");
        }

        Node<Item> oldFirst = first;
        first = first.next;
        if (first == null) {
            last = null;
        } else {
            first.previous = null;
        }
        size--;

        return oldFirst.value;
    }

    /**
     * remove and return the item from the end
     */
    public Item removeLast() {
        if (last == null) {
            throw new NoSuchElementException("Client tries to remove an Item from empty deque.");
        }

        Node<Item> oldLast = last;
        last = oldLast.previous;
        if (last == null) {
            first = null;
        } else {
            last.next = null;
        }
        size--;

        return oldLast.value;
    }

    /**
     * return an iterator over items in order from front to end
     */
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private static class Node<Item> {
        private Item value;
        private Node<Item> next;
        private Node<Item> previous;
    }

    // an iterator, doesn't implement remove() since it's optional
    private class DequeIterator implements Iterator<Item> {
        private Node<Item> current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException("No next element available. Reached end of deque.");
            }

            Item value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove is not supported");
        }
    }

    /**
     * unit testing (optional)
     */
    public static void main(String[] args) {

    }
}

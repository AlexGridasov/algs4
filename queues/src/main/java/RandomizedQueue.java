import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * User: Alex
 * Date: 16.02.17
 */
public class RandomizedQueue<Item> implements Iterable<Item> {

    private static final int MINIMUM_STORAGE_SIZE = 2;

    private Item[] items;
    private int size;

    /**
     * construct an empty randomized queue
     */
    public RandomizedQueue() {
        items = (Item[]) new Object[MINIMUM_STORAGE_SIZE];
        size = 0;
    }

    /**
     * is the queue empty?
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * return the number of items on the queue
     */
    public int size() {
        return size;
    }

    /**
     * add the item
     */
    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException("It is not allowed to enqueue null values.");
        }

        if (items.length == size) {
            resizeStorage(items.length * 2);
        }

        items[size++] = item;
    }

    /**
     * remove and return a random item
     */
    public Item dequeue() {
        if (size == 0) {
            throw new NoSuchElementException("Trying to dequeue an item from an empty RandomQueue.");
        }

        int index = StdRandom.uniform(size);
        Item value = items[index];
        size--;
        items[index] = items[size];
        items[size] = null;

        if (items.length > MINIMUM_STORAGE_SIZE
                && size <= items.length / 4) {
            resizeStorage(items.length / 2);
        }

        return value;
    }

    /**
     * return (but do not remove) a random item
     */
    public Item sample() {
        if (size == 0) {
            throw new NoSuchElementException("Trying to sample an item from an empty RandomQueue.");
        }

        int index = StdRandom.uniform(size);
        return items[index];
    }

    /**
     * return an independent iterator over items in random order
     */
    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    private void resizeStorage(int capacity) {
        assert capacity >= size;
        items = java.util.Arrays.copyOf(items, capacity);
    }

    private class RandomIterator implements Iterator<Item> {

        private Item[] items;
        private int index;

        public RandomIterator() {
            items = copyRandomQueueItems();
            StdRandom.shuffle(items);
        }

        private Item[] copyRandomQueueItems() {
            return java.util.Arrays.copyOf(RandomizedQueue.this.items, size);
        }

        @Override
        public boolean hasNext() {
            return index < items.length;
        }

        @Override
        public Item next() {
            if (hasNext()) {
                return items[index++];
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove is not supported");
        }
    }

    /**
     * unit testing (optional)
     */
    public static void main(String[] args) {

    }
}

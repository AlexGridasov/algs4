import edu.princeton.cs.algs4.StdIn;

/**
 * User: Alex
 * Date: 16.02.17
 */
public class Permutation {

    /**
     * One integer command-line argument k
     * and it is between 0 and the number of strings on standard input
     */
    public static void main(String[] args) {
        String[] strings = StdIn.readAllStrings();

        int k = Integer.parseInt(args[0]);

        RandomizedQueue<String> randomizedQueue = new RandomizedQueue<>();
        for (String string : strings) {
            randomizedQueue.enqueue(string);
        }

        for (int i = 0; i < k; i++) {
            System.out.println(randomizedQueue.dequeue());
        }
    }
}

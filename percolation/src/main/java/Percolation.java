import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * User: Alex
 * Date: 12.02.2017
 */
public class Percolation {

    private int size;
    private boolean[] openedSites;
    private int vTop;
    private int vBottom;
    private WeightedQuickUnionUF unionUF;

    /**
     * create n-by-n grid, with all sites blocked
     */
    public Percolation(int n) {
        if (n <= 0)
            throw new IndexOutOfBoundsException("index out of bounds");

        this.size = n;
        this.vTop = 0;
        this.vBottom = n * n + 1;
        this.openedSites = new boolean[n * n + 2];
        this.unionUF = new WeightedQuickUnionUF(n * n + 2);
    }

    /**
     * open site (row, col) if it is not open already
     */
    public void open(int row, int col) {
        if (isParamsNotInBounds(row, col))
            throw new IndexOutOfBoundsException("index out of bounds");

        // mark the site as open.
        int index = xyTo1D(row, col);
        openedSites[index] = true;

        // perform some sequence of WeightedQuickUnionUF operations that links the site in question to its open neighbors.
        if (row == 1) {
            unionUF.union(index, vTop);
        }
        if (row == size) {
            unionUF.union(index, vBottom);
        }

        if (col > 1 && isOpen(row, col - 1)) {
            // if the left site is open
            unionUF.union(index, xyTo1D(row, col - 1));
        }
        if (col < size && isOpen(row, col + 1)) {
            // if the right site is open
            unionUF.union(index, xyTo1D(row, col + 1));
        }
        if (row > 1 && isOpen(row - 1, col)) {
            // if the top site is open
            unionUF.union(index, xyTo1D(row - 1, col));
        }
        if (row < size && isOpen(row + 1, col)) {
            // if the bottom site is open
            unionUF.union(index, xyTo1D(row + 1, col));
        }
    }

    /**
     * is site (row, col) open?
     */
    public boolean isOpen(int row, int col) {
        if (isParamsNotInBounds(row, col))
            throw new IndexOutOfBoundsException("index out of bounds");

        return openedSites[xyTo1D(row, col)];
    }

    /**
     * is site (row, col) full?
     */
    public boolean isFull(int row, int col) {
        if (isParamsNotInBounds(row, col))
            throw new IndexOutOfBoundsException("index out of bounds");

        return unionUF.connected(vTop, xyTo1D(row, col));
    }

    /**
     * number of open sites
     */
    public int numberOfOpenSites() {
        int number = 0;
        for (int i = 0; i < openedSites.length; i++) {
            if (openedSites[i]) {
                number++;
            }
        }
        return number;
    }

    /**
     * does the system percolate?
     */
    public boolean percolates() {
        return unionUF.connected(vTop, vBottom);
    }

    // validate the indices of the site that it receives.
    private boolean isParamsNotInBounds(int row, int col) {
        return (row <= 0 || row > size
                || col <= 0 || col > size);
    }

    // map from a 2-dimensional (row, column) pair to a 1-dimensional union find object index
    private int xyTo1D(int row, int col) {
        int result = col + size * (row - 1);
//        System.out.println(String.format("{row = %d, col = %d, 1D = %d}", row, col, result));
        return result;
    }

    /**
     * test client (optional)
     */
    public static void main(String[] args) {

    }
}

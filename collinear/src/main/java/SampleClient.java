import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

/**
 * User: Alex
 * Date: 23/2/17
 */
public class SampleClient {

    public static void main(String[] args) {
        // read the N points from a file
//        Point[] points = getPointsFromTestFile(args[0]);
        Point[] points = getPointsFromTestFile("input200");

        // draw the points
        StdDraw.show(0);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }

    public static Point[] getPointsFromTestFile(String fileName) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        In in = new In(classloader.getResource("collinear/" + fileName + ".txt"));      // input file

//        In in = new In("test-input/" + fileName);
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }
        return points;
    }

}
